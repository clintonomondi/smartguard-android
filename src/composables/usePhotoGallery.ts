import { ref, onMounted, watch } from 'vue';
import {Plugins, CameraResultType, CameraSource, CameraDirection} from "@capacitor/core";
import  Input  from '../views/Input.vue';
// export function usePhotoGallery() {
    const { Camera } = Plugins;
//     const takePhoto = async () => {
//         const cameraPhoto = await Camera.getPhoto({
//             resultType: CameraResultType.DataUrl,
//             source: CameraSource.Camera,
//             quality: 100,
//             height:600,
//             width:600,
//             direction:CameraDirection.Rear
//         });
//          // const pic=await cameraPhoto.base64String as string;
//          // Input?.methods?.dataURItoBlob(pic);
//     };
//     return {takePhoto};
// }

export class ApiService {
    static  async  takePhoto() {
        const image = await Camera.getPhoto({
            quality: 100,
            resultType: CameraResultType.DataUrl,
            correctOrientation: true,
            direction: CameraDirection.Rear,
            height: 600,
            width: 600,
            source: CameraSource.Camera,
        });
        // Can be set to the src of an image now
        return image;
    }
}