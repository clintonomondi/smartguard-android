import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Tabs from '../views/Tabs.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/tabs/login'
  },
  {
    path: '/tabs/login',
    component: () => import('@/views/Login.vue')
  },
  {
    path: '/tabs/changepassword',
    component: () => import('@/views/ChangePassword.vue')
  },
    {
        path: '/tabs/verify',
        component: () => import('@/views/Verify.vue')
    },
    {
        path: '/tabs/setpassword',
        component: () => import('@/views/setpassword.vue')
    },
    {
        path: '/tabs/forget',
        component: () => import('@/views/Forget.vue')
    },
  {
    path: '/tabs/',
    component: Tabs,
    children: [
      {
        path: 'home',
        component: () => import('@/views/Home.vue')
      },
      {
        path: 'input',
        component: () => import('@/views/Input.vue')
      },
      {
        path: 'profile',
        component: () => import('@/views/Profile.vue')
      },
      {
        path: 'history',
        component: () => import('@/views/History.vue')
      },

    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
